function binSearch(arr, n, i = 0, j = arr.length-1) {
    const mid = Math.floor( (i + j) / 2 )
    let result

    if (arr[mid] > n) {
        result = binSearch(arr, n, i, mid)
    } else if (arr[mid] < n) {
        result = binSearch(arr, n, mid+1, j)
    } else {
        result = mid
    }
    
    return result

}

function quickSort(arr) {
    if (arr.length < 2) {
        return arr
    }

    const pivot = Math.floor(arr.length / 2)
    const left = []
    const right = []

    if (arr.length === 2) {
        if (arr[0] > arr[1]) {
            return [arr[1], arr[0]]
        } else {
            return arr
        }
    } else {
        for (let i=0; i<arr.length; i++) {
            if (i !== pivot) {
                if (arr[i] <= arr[pivot]) {
                    left.push(arr[i])
                } else {
                    right.push(arr[i])
                }
            }
        }
    }

    return [].concat(quickSort(left), arr[pivot], quickSort(right))
}

console.log(quickSort([-400, 76534, 36, 3, 46543, 8, 6, 5, 18, 2, 40, 36, 54334, 3556, 4, 54, 67545, 1]))
