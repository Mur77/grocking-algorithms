/**
 *                      |-5-> wussy -2-|
 *        |-4-> kissi --|         ^    |--> krishni
 *        |             |-8-> sissi -4-|
 *        |                       |    |
 * main --|-2-> lisi -3-> mishni -1    |
 *        |                            |
 *        |             |-6-> myaso -1-|
 *        |-1-> lilli --|
 *                      |-3-> fish
 */

const graph = {
    main: {
        mango: false,
        neighbours: [
            { name: 'kissi', weight: 2, },
            { name: 'lisi', weight: 2, },
            { name: 'lilli', weight: 1, }
        ],
    },
    kissi: {
        mango: false,
        neighbours: [
            { name: 'wussy', weight: 5, },
            { name: 'sissi', weight: 8, },
        ],
    },
    lisi: {
        mango: false,
        neighbours: [
            { name: 'mishni', weight: 3, },
        ],
    },
    lilli: {
        mango: false,
        neighbours: [
            { name: 'myaso', weight: 6, },
            { name: 'fish', weight: 3, },
        ],
    },
    mishni: {
        mango: false,
        neighbours: [
            { name: 'wussy', weight: 1, },
        ],
    },
    sissi: {
        mango: false,
        neighbours: [
            { name: 'krishni', weight: 4, },
        ],
    },
    wussy: {
        mango: false,
        neighbours: [
            { name: 'krishni', weight: 2, },
        ],
    },
    myaso: {
        mango: true,
        neighbours: [
            { name: 'krishni', weight: 1, },
        ],
    },
    fish: {
        mango: false,
        neighbours: [],
    },
    krishni: {
        mango: false,
        neighbours: [],
    },
}

function widthSearch(graph) {
    const searchQueue = [].concat(graph.main.neighbours.map(item => item.name))
    const checked = []

    while (searchQueue.length > 0) {
        const friend = searchQueue.shift()

        if (!checked.find(item => item === friend)) {
            checked.push(friend)

            if (graph[friend].mango) {
                return friend
            }

            graph[friend].neighbours.forEach(element => {
                searchQueue.push(element.name)
            });
        }
    }

    return 'No mango'
}

function findWay(graph, start, target, searchWay = []) {
    const searchQueue = [].concat(graph[start].neighbours.map(item => item.name))

    // Сначала просматриваем текущий уровень
    for(let i=0; i<searchQueue.length; i++) {
        const friend = searchQueue[i]

        if (friend === target) {
            return searchWay.concat(friend)
        }
    }
    
    // Если в текущем уровне цели нет, просматриваем соседей
    for (let i=0; i<searchQueue.length; i++) {
        const friend = searchQueue[i]
        const way = findWay(graph, friend, target, searchWay.concat(friend))
        if (way[way.length-1] === target) {
            return way
        }
    }

    return 'No way'
}

function daxtre(graph, start, target) {

    function findLowestCostNode() {
        let lowestCost = Infinity
        let lowestCostNode = ''

        for (let item in costs) {
            const cost = costs[item]

            if (cost < lowestCost && !processed[item]) {
                lowestCost = cost
                lowestCostNode = item
            }
        }

        return lowestCostNode
    }

    const costs = {}
    graph[start].neighbours.forEach(item => costs[item.name] = item.weight)
    if (!costs[target]) {
        costs[target] = Infinity
    }

    const parents = {}
    graph[start].neighbours.forEach(item => parents[item.name] = start)
    if (!parents[target]) {
        parents[target] = ''
    }

    const processed = {}

    // Находим узел с минимальной стоимостью
    let node = findLowestCostNode(costs)

    while (node && node !== target) {

        // Стоимость этого узла
        const cost = costs[node]

        // Соседи этого узла    
        const neighbours = graph[node].neighbours

        // Если neighbours пустой, значит у узла node нет выхода на target
        if (Object.keys(neighbours).length === 0) {
            costs[node] = Infinity
        } else {
            // Перебираем соседей
            neighbours.forEach(item => {
                // Вычисляем стоимость маршрута  start-node-item
                const newCost = cost + item.weight

                // Сравним стоимость start-item и start-node-item
                // Если узла item еще нет в costs, тогда просто добавляем
                if (!costs[item.name]) {
                    costs[item.name] = newCost
                    parents[item.name] = node
                }
                if (costs[item.name] > newCost) {
                    // Сохраняем новую меньшую стоимость
                    costs[item.name] = newCost
                    // и назначаем node новым родителем для item
                    parents[item.name] = node
                }
            })
        }

        // Помечаем узел node как обработанный
        processed[node] = 1

        // Находим следующий узел с минимальной стоимостью
        node = findLowestCostNode(costs)
    }

    // Формируем массив узлов самого короткого пути
    const way = []
    way.push(target)
    node = target
    while (node !== start) {
        node = parents[node]
        way.unshift(node)
    }

    return way
}

//console.log(widthSearch(graph))
//console.log(findWay(graph, 'main', 'krishni'))
console.log(daxtre(graph, 'main', 'krishni'));
